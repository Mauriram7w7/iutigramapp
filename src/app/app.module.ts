import { BrowserModule } from '@angular/platform-browser';
import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';


import { AppComponent } from './app.component';
import { AppRoutingModule } from './/app-routing.module';
import { NavbarComponent } from './components/navbar/navbar.component';
import { InicioComponent } from './pages/inicio/inicio.component';
import { PerfilComponent } from './pages/perfil/perfil.component';
import { RegistroComponent } from './pages/registro/registro.component';


const rutas: Routes = [
  {path: 'Inicio', component: InicioComponent},
  {path: 'Perfil', component: PerfilComponent},
  {path: 'Registro', component: RegistroComponent},
  {path: '', redirectTo: '/Inicio', pathMatch: 'full'},
];



@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    InicioComponent,
    PerfilComponent,
    RegistroComponent,
  
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(
      rutas,
      {enableTracing: true }
    )
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
